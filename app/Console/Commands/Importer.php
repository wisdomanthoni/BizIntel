<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WpApi;

class Importer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:wordpress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull wordpress data';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $wp;
    
    public function __construct(WpApi $wp)
    {
        parent::__construct();
        $this->wpApi = $wp;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $page = ($this->argument('page')) ? $this->argument('page') : 1;
        $this->wpApi->importPosts($page);
    }
}
