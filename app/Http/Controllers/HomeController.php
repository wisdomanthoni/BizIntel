<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \LeeOvery\WordpressToLaravel\Post;
use \LeeOvery\WordpressToLaravel\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Post::orderBy('published_at', 'desc')->get()->random(4);

        //return $posts;
        $category = Category::whereSlug('investment')->firstOrFail();

        // to fetch newest 5 posts (paginated) by category slug (from above)...
        $pos = Post::whereCategory($category->slug)
                     ->orderBy('published_at', 'desc')
                     ->get()
                     ->random(4);

        //return $pos;  

        return view('welcome',[
           'posts' => $posts,
           'investments' => $pos
        ]);
    }
}
