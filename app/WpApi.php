<?php

namespace App;

use App\Post;
use App\Category;
use App\User;

class WpApi {

	protected $url = 'https://bizintelng.com/wp-json/wp/v2/';

    public function importPosts($page = 1)
    {
        $posts = collect($this->getJson($this->url . 'posts/?_embed&filter[orderby]=modified&page=' . $page));
        //return $posts;
        foreach ($posts as $post) {
            $this->syncPost($post);
        }
    }

    protected function getJson($url)
    {
        $response = file_get_contents($url, false);
        return json_decode( $response );
    }

        protected function syncPost($data)
    {
        $found = Post::where('wp_id', $data->id)->first();

        if (! $found) {
            return $this->createPost($data);
        }

        if ($found and $found->updated_at->format("Y-m-d H:i:s") < $this->carbonDate($data->modified)->format("Y-m-d H:i:s")) {
            return $this->updatePost($found, $data);
        }
    }

    protected function carbonDate($date)
    {
        return Carbon::parse($date);
    }

    protected function createPost($data)
    {
        $post = new Post();
        $post->id = $data->id;
        $post->wp_id = $data->id;
        $post->user_id = $this->getAuthor($data->author);
        $post->title = $data->title->rendered;
        $post->slug = $data->slug;
        $post->featured_image = $this->featuredImage($data->_embedded);
        $post->featured = ($data->sticky) ? 1 : null;
        $post->excerpt = $data->excerpt->rendered;
        $post->content = $data->content->rendered;
        $post->format = $data->format;
        $post->status = 'publish';
        $post->publishes_at = $this->carbonDate($data->date);
        $post->created_at = $this->carbonDate($data->date);
        $post->updated_at = $this->carbonDate($data->modified);
        $post->category_id = $this->getCategory($data->_embedded->{"wp:term"});
        $post->save();
        $this->syncTags($post, $data->_embedded->{"wp:term"});
        return $post;
    }

    public function featuredImage($data)
    {
        if (property_exists($data, "wp:featuredmedia")) {
            $data = head($data->{"wp:featuredmedia"});
            if (isset($data->source_url)) {
                return $data->source_url;
            }
        }
        return null;
    }

    public function getCategory($data)
    {
        $category = collect($data)->collapse()->where('taxonomy', 'category')->first();
        $found = Category::where('wp_id', $category->id)->first();
        if ($found) {
            return $found->id;
        }
        $cat = new Category();
        $cat->id = $category->id;
        $cat->wp_id = $category->id;
        $cat->name = $category->name;
        $cat->slug = $category->slug;
        $cat->description = '';
        $cat->save();
        return $cat->id;
    }

    public function getAuthor($data)
    {
    	$found = User::where('wp_id', $data)->first();

    	if (! $found) {
    		$u = collect($this->getJson($this->url . 'user/' . $data));
    		$user = User::create([
			            'name' => $data['name'],
			            'wp_id' => $data,
			            'email' => $data['email'],
			            'password' => Hash::make('supersecret'),
			        ]);

            return $user->id;
        }

        return $found->id;
    }

    private function syncTags(Post $post, $tags)
    {
        $tags = collect($tags)->collapse()->where('taxonomy', 'post_tag')->pluck('name')->toArray();
        if (count($tags) > 0) {
            $post->setTags($tags);
        }
    }

} 