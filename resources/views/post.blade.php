@extends('layouts.master')

@section('content')
<style type="text/css">
	              /*
 * Blog name and description
 */
.blog-title {
  margin-bottom: 0;
  font-size: 2rem;
  font-weight: 400;
}
.blog-description {
  font-size: 1.1rem;
  color: #999;
}

@media (min-width: 40em) {
  .blog-title {
    font-size: 3.5rem;
  }
}

/* Pagination */
.blog-pagination {
  margin-bottom: 4rem;
}
.blog-pagination > .btn {
  border-radius: 2rem;
}

/*
 * Blog posts
 */
.blog-post {
  margin-bottom: 4rem;
}
.blog-post-title {
  margin-bottom: .25rem;
  font-size: 2.5rem;
}
.blog-post-meta {
  margin-bottom: 1.25rem;
  color: #999;
}

		.btn-default {
		    color: #333;
		    background-color: #3DA368;
		    border-color: #009688;
		    border-radius:0px;
		    color:#fff;
		}

		#blog-section{margin-top:40px;margin-bottom:80px;}
		.content-title{padding:5px;background-color:#fff;}
		.content-title h5 a{color:#34495E;text-decoration:none; transition: 0.5s;}
		.content-title h5 a:hover{color:#000; }
		.content-footer{background-color:#239fbc;padding:10px;position: relative;}
		.content-footer span a {
		    color: #fff;
		    display: inline-block;
		    padding: 6px 5px;
		    text-decoration: none;
		    transition: 0.5s;
		}
		.content-footer span a:hover{     
		    color:#f0f9fb;   
		}
		aside{
		    margin-top: 30px;
		    -webkit-box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);
		-moz-box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);
		box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);}
		aside .content-footer>img {
		    width: 33px;
		    height: 33px;
		    border-radius: 100%;
		    margin-right: 10px;
		    border: 2px solid #fff;
		}

		.user-ditels {
		    width: 300px;
		    top: -100px;
		    height: 100px;
		    padding-bottom: 99px;
		    position: absolute;
		    border: solid 2px #fff;
		    background-color: grey;
		    right: 25px;
		    display: none;
		    z-index: 1;
		}

		@media (max-width:768px){
		    .user-ditels {   
		    right: 5px;   
		}
		    
		}
		.user-small-img{cursor: pointer;}

		.content-footer:hover .user-ditels  {
		    display: block;
		}


		.content-footer .user-ditels .user-img{width: 100px;height:100px;float: left;}
		.user-full-ditels h3 {
		    color: #fff;
		    display: block;
		    margin: 0px;
		    padding-top: 10px;
		    padding-right: 28px;
		    text-align: right;
		}
		.user-full-ditels p{    
		    color: #fff;
		    display: block;
		    margin: 0px;
		     padding-right: 20px;
		    padding-top: 5px;
		   text-align: right;
		}

		/*recent-post-col////////////////////*/
		.widget-sidebar {
		    background-color: #fff;
		    padding: 20px;
		    margin-top: 30px;
		}

		.title-widget-sidebar {
		    font-size: 14pt;
		    border-bottom: 2px solid #e5ebef;
		    margin-bottom: 15px;
		    padding-bottom: 10px;    
		    margin-top: 0px;
		}

		.title-widget-sidebar:after {
		    border-bottom: 2px solid #239fbc;
		    width: 150px;
		    display: block;
		    position: absolute;
		    content: '';
		    padding-bottom: 10px;
		}

		.recent-post{width: 100%;height: 80px;list-style-type: none;}
		.post-img img {
		    width: 100px;
		    height: 70px;
		    float: left;
		    margin-right: 15px;
		    border: 5px solid #239fbc;
		    transition: 0.5s;
		}

		.recent-post a {text-decoration: none;color:#34495E;transition: 0.5s;}
		.post-img, .recent-post a:hover{color:#F39C12;}
		.post-img img:hover{border: 5px solid #F39C12;}

		/*===============ARCHIVES////////////////////////////*/



		button.accordion {
		    background-color: #16A085;
		    color: #fff;
		    cursor: pointer;
		    padding: 18px;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #F39C12;color: #fff;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #fff;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		.panel {
		    padding: 0 18px;
		    background-color: white;
		    max-height: 0;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}


		/*categories//////////////////////*/

		.categories-btn{
		    background-color: #F39C12;
		    margin-top:30px;
		    color: #fff;
		    cursor: pointer;
		    padding: 18px;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    transition: 0.4s;
		    
		}
		.categories-btn:after {
		    content: '\25BA';
		    color: #fff;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}
		.categories-btn:hover {
		    background-color: #16A085;color: #fff;
		}

		.form-control{border-radius: 0px;}

		.btn-warning {
		    border-radius: 0px;
		    background-color: #F39C12;
		    margin-top: 15px;
		}
		.input-group-addon{border-radius: 0px;}
</style>


<div class="container">
    
</div>
 <section id="blog-section">
     <div class="container">
       <div class="row">
         <div class="col-lg-8">
           <div class="row">
        <div class="col blog-main">
          {{-- <h3 class="pb-3 mb-4 font-italic border-bottom">
            From the Firehose
          </h3> --}}

          <div class="blog-post">
            <h2 class="blog-post-title">{!! $post->title !!}</h2>
            <p class="blog-post-meta">{{ Carbon::parse($post->created_at)->diffForHumans()}} <a href="#" class="text-capitalize">{{$post->author->name}}</a><img src="{{$post->author->avatar}}" class="img-fluid img-thumbnail mx-2"height="25" width="25"></p>

          @isset ($post->featured_image)
            <img src="{{$post->featured_image}}" class="img-fluid figure-img w-100">
          @else
          @endisset
            {!! $post->content !!}
             
          </div><!-- /.blog-post -->

         
          <nav class="blog-pagination">
          	@forelse($post->tags as $tag)
            <a class="btn btn-outline-primary" href="{{ url('/blog/tag/'.$tag->slug) }}">{{$tag->name}}</a>
            @empty
              <a class="btn btn-outline-secondary disabled" href="#">No Tag</a>
            @endforelse
          </nav>
        </div><!-- /.blog-main -->

               
               
           </div>
          </div>

         <div class="col-lg-4"> 
                      
               <div class="widget-sidebar">
                 <h2 class="title-widget-sidebar">RECENT POST</h2>
                   <div class="content-widget-sidebar">
                    <ul>
                     @foreach($random as $other )
	                     <li class="recent-post">
	                        <div class="post-img" style="float: left; clear: left;">
	                           <img src="{{$other->featured_image or asset('img/investment-1.png') }}" class="img-fluid" >
	                         </div>
	                         <a href="{{ url('/blog/'.$other->slug) }}"><p class="small" style="line-height: px;">{!! str_limit($other->title ,50)!!}</p></a>
	       
	                      </li>
                        <hr>
                       @endforeach        
                    </ul>
                   </div>
                 </div>
             
               <!--=====================
                    TAGS
             ======================-->
				<div class="widget-sidebar">
				 <h2 class="title-widget-sidebar">CATEGORIES</h2>
					 @foreach($categories as $cat)
					 <a class="btn btn-outline-primary small btn-sm text-lowercase" href="{{ url('/blog/category/'.$cat->slug) }}">{!! $cat->name !!}</a>
	                 @endforeach
			    </div> 
      
              <!--=====================
                    NEWSLATTER
             ======================-->
				<div class="widget-sidebar">
				 <h2 class="title-widget-sidebar">NEWSLETTER</h2>
				  <p>Subscribe Now for Loaded Business Building Tips.</p>  
				    <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
						  </div>
						   <input id="email" type="text" class="form-control" name="email" placeholder="Email">
					</div>
				    <button type="button" class="btn" style="background-color: #239fbc; color: #fff;">SEND</button>
			    </div>  
                 
                 
             </div>
           </div>
         </div>
     
    </section>

       
<br>
<br>
<br>

@endsection
