@extends('layouts.master')

@section('content')
<style type="text/css">
	
		.btn-default {
		    color: #333;
		    background-color: #3DA368;
		    border-color: #009688;
		    border-radius:0px;
		    color:#fff;
		}

		#blog-section{margin-top:40px;margin-bottom:80px;}
		.content-title{padding:5px;background-color:#fff;}
		.content-title h5 a{color:#34495E;text-decoration:none; transition: 0.5s;}
		.content-title h5 a:hover{color:#000; }
		.content-footer{background-color:#239fbc;padding:10px;position: relative;}
		.content-footer span a {
		    color: #fff;
		    display: inline-block;
		    padding: 6px 5px;
		    text-decoration: none;
		    transition: 0.5s;
		}
		.content-footer span a:hover{     
		    color:#f0f9fb;   
		}
		aside{
		    margin-top: 30px;
		    -webkit-box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);
		-moz-box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);
		box-shadow: 1px 4px 16px 3px rgba(199,197,199,1);}
		aside .content-footer>img {
		    width: 33px;
		    height: 33px;
		    border-radius: 100%;
		    margin-right: 10px;
		    border: 2px solid #fff;
		}

		.user-ditels {
		    width: 300px;
		    top: -100px;
		    height: 100px;
		    padding-bottom: 99px;
		    position: absolute;
		    border: solid 2px #fff;
		    background-color: grey;
		    right: 25px;
		    display: none;
		    z-index: 1;
		}

		@media (max-width:768px){
		    .user-ditels {   
		    right: 5px;   
		}
		    
		}
		.user-small-img{cursor: pointer;}

		.content-footer:hover .user-ditels  {
		    display: block;
		}


		.content-footer .user-ditels .user-img{width: 100px;height:100px;float: left;}
		.user-full-ditels h3 {
		    color: #fff;
		    display: block;
		    margin: 0px;
		    padding-top: 10px;
		    padding-right: 28px;
		    text-align: right;
		}
		.user-full-ditels p{    
		    color: #fff;
		    display: block;
		    margin: 0px;
		     padding-right: 20px;
		    padding-top: 5px;
		   text-align: right;
		}

		/*recent-post-col////////////////////*/
		.widget-sidebar {
		    background-color: #fff;
		    padding: 20px;
		    margin-top: 30px;
		}

		.title-widget-sidebar {
		    font-size: 14pt;
		    border-bottom: 2px solid #e5ebef;
		    margin-bottom: 15px;
		    padding-bottom: 10px;    
		    margin-top: 0px;
		}

		.title-widget-sidebar:after {
		    border-bottom: 2px solid #239fbc;
		    width: 150px;
		    display: block;
		    position: absolute;
		    content: '';
		    padding-bottom: 10px;
		}

		.recent-post{width: 100%;height: 80px;list-style-type: none;}
		.post-img img {
		    width: 100px;
		    height: 70px;
		    float: left;
		    margin-right: 15px;
		    border: 5px solid #239fbc;
		    transition: 0.5s;
		}

		.recent-post a {text-decoration: none;color:#34495E;transition: 0.5s;}
		.post-img, .recent-post a:hover{color:#F39C12;}
		.post-img img:hover{border: 5px solid #F39C12;}

		/*===============ARCHIVES////////////////////////////*/



		button.accordion {
		    background-color: #16A085;
		    color: #fff;
		    cursor: pointer;
		    padding: 18px;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #F39C12;color: #fff;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #fff;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		.panel {
		    padding: 0 18px;
		    background-color: white;
		    max-height: 0;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}


		/*categories//////////////////////*/

		.categories-btn{
		    background-color: #F39C12;
		    margin-top:30px;
		    color: #fff;
		    cursor: pointer;
		    padding: 18px;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    transition: 0.4s;
		    
		}
		.categories-btn:after {
		    content: '\25BA';
		    color: #fff;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}
		.categories-btn:hover {
		    background-color: #16A085;color: #fff;
		}

		.form-control{border-radius: 0px;}

		.btn-warning {
		    border-radius: 0px;
		    background-color: #F39C12;
		    margin-top: 15px;
		}
		.input-group-addon{border-radius: 0px;}
</style>
 <div class="middle-nav px-5">
 	<div class="nav-scroller py-1 mb-2">
        <nav class="nav d-flex justify-content-between">
        	@foreach($tags->take(11) as $t)
               <a class="p-2 text-muted text-capitalize" href="{{ url('/blog/tag/'.$t->slug) }}">{!!$t->name !!}</a>
        	@endforeach
          
          {{-- <a class="p-2 text-muted" href="#">U.S.</a>
          <a class="p-2 text-muted" href="#">Technology</a>
          <a class="p-2 text-muted" href="#">Design</a>
          <a class="p-2 text-muted" href="#">Culture</a>
          <a class="p-2 text-muted" href="#">Business</a>
          <a class="p-2 text-muted" href="#">Politics</a>
          <a class="p-2 text-muted" href="#">Opinion</a>
          <a class="p-2 text-muted" href="#">Science</a>
          <a class="p-2 text-muted" href="#">Health</a>
          <a class="p-2 text-muted" href="#">Style</a>
          <a class="p-2 text-muted" href="#">Travel</a> --}}
        </nav>
    </div>
 </div>

<div class="container">
    
</div>
 <section id="blog-section">
     <div class="container">
       <div class="row">
         <div class="col-lg-8">
           <div class="row"> 
               @foreach($posts as $post)
                      <div class="col-lg-6 col-md-6">
			             <aside>
			                <img src="{{$post->featured_image or asset('img/investment-1.png') }}"  class=" img-fluid w-100" style="height: 200px;">
			                <div class="content-title">
							<div class="text-center">
							<h5><a href="{{ url('/blog/'.$post->slug) }}"> {!! $post->title !!}</a></h5>
							</div>
							</div>
							<div class="content-footer">
							
							<span style="font-size: 16px;color: #fff;"><a href="{{ url('/blog/'.$post->slug) }}">Read More</a></span>
							<span class="pull-right">
							</span>
			                    {{-- <div class="user-ditels">
			                        <div class="user-full-ditels">
			                        <p>{!!str_limit($post->excerpt,120)!!}</p>
			                        </div>
			                    </div> --}}
							</div>
			             </aside>
			           </div>
               @endforeach
               <div class="m-5 my-5">
               	  {{ $posts->links() }}
               </div>
           </div>
          </div>

         <div class="col-lg-4"> 
                       
               <div class="widget-sidebar">
                 <h2 class="title-widget-sidebar">RECENT POST</h2>
                   <div class="content-widget-sidebar">
                    <ul>
                     @foreach($random as $other )
                     <li class="recent-post">
                        <div class="post-img">
                         <img src="{{$other->featured_image or asset('img/investment-1.png') }}" class="img-fluid">
                         </div>
                         <a href="{{ url('/blog/'.$other->slug) }}"><p><small style="line-height: px;">{!! str_limit($other->title ,50)!!}</small></p></a>
                        {{--  <p><small><i class="fa fa-calendar" data-original-title="" title=""></i>{{$other->created_at->diffForHumans()}}</small></p> --}}
                        </li>
                        <hr>
                       @endforeach        
                    </ul>
                   </div>
                 </div>
             
               <!--=====================
                    TAGS
             ======================-->
				<div class="widget-sidebar">
				 <h2 class="title-widget-sidebar">TAGS</h2>
					 @foreach($tags as $tag)
					 <a class="btn btn-outline-primary small btn-sm text-lowercase" href="{{ url('/blog/tag'.$tag->slug) }}">{!! $tag->name !!}</a>
	                 @endforeach
			    </div> 
      
              <!--=====================
                    NEWSLATTER
             ======================-->
				<div class="widget-sidebar">
				 <h2 class="title-widget-sidebar">NEWSLETTER</h2>
				  <p>Subscribe Now for Loaded Business Building Tips.</p>  
				    <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
						  </div>
						   <input id="email" type="text" class="form-control" name="email" placeholder="Email">
					</div>
				    <button type="button" class="btn" style="background-color: #239fbc; color: #fff;">SEND</button>
			    </div>  
                 
                 
             </div>
           </div>
         </div>
     
    </section>

       
<br>
<br>
<br>

@endsection
