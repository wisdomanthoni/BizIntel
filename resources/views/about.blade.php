@extends('layouts.master')

@section('content')
      
        <div class="jumbotron jumbotron-fluid about-jumbo">
                <div class="container text-white">
                  <div class="row">
                  <span class="hor-line mt-1 my-1">
                  </span> {{-- <span class="display-6 jumbo-text">BUSINESS MINDED</span> --}}
                </div>
                  <h1 class="display-4 pl-5">Bring Your Business Ideas<br> to Life</h1>
                  <div class="row description">
                  <div class="hor-line"></div>
                  <small class="jumbo-text pb-5 pt-2 pl-4">Bizintelng is your indispensable source for Business and investment consulting.
                   <br> We provide you with the latest breaking news and videos straight from business <br> in perspectve coming from different sectors.</small>
                  </div>
                  {{-- <div class="text-center pt-5 scroll">SCROLL</div> --}}
                 <div class="vert-line pt-4"></div>
                </div>
              </div>

  <section id="about-us">
                  <div class="container pt-5 pb-5">
                      <h3 class="text-center  ">About BizIntelng</h3>
                      <hr class="hr">
                      <div class="row pt-4">
                          <div class="col-md-6">
                              <div class="card" style="width: 30rem; max-width: 100%;">
                                  <img class="card-img-top img-fluid" style= "height: 540px;" src="img/meeting.jpeg" alt="About photo">
                          </div>
                          </div>
                          <div class="col-md-6">
                              <p class="text-justify">Business Intelligence Nigeria provides you with essential business information and business opportunities in Nigeria that you need to succeed in your business.
We promise to track and research every source to bring you those local economic  business  news and insights that really matter.
We also provide periodic analysis of the Nigeria business environment so you can better appreciate and take advantage of the information to advance your business.

</p>
                                <h4 class= "statement">Mission</h4>
                                <p class="text-justify">Business Intelligence Nigeria provides you with essential business information and business opportunities in Nigeria that you need to succeed in your business.
We promise to track and research every source to bring you those local economic  business  news and insights that really matter.
We also provide periodic analysis of the Nigeria business environment so you can better appreciate and take advantage of the information to advance your business.

</p>
                                  <h4 class= "statement">Vision</h4>
                                  <p class="text-justify">Business Intelligence Nigeria provides you with essential business information and business opportunities in Nigeria that you need to succeed in your business.
We promise to track and research every source to bring you those local economic  business  news and insights that really matter.
We also provide periodic analysis of the Nigeria business environment so you can better appreciate and take advantage of the information to advance your business.
</p>
                          </div>
                      </div>
                     
                  </div>
  </section>

  <section id="our-team">
    <div class="container pt-5 pb-5">
        <h3 class="text-center  ">Our Team</h3>
        <hr class="hr">
        <div class="row pt-4">
            <div class="col-md-4 mb-5">
              <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="img/pexels2.jpeg" alt="Team member photo">
                <div class="card-body">
                  <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
                  <div class="card-text text-center status" style= "font-size: 12px;">Creative Director</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-5">
              <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="img/beard-boy.jpeg" alt="Team member photo">
                <div class="card-body">
                  <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
                  <div class="card-text text-center status" style= "font-size: 12px;">Creative Director</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-5">
              <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="img/pexels.jpeg" alt="Team member photo">
                <div class="card-body">
                  <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
                  <div class="card-text text-center status" style= "font-size: 12px;">Creative Director</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-5">
              <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="img/pexels6.jpeg" alt="Team member photo">
                <div class="card-body">
                  <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
                  <div class="card-text text-center status" style= "font-size: 12px;">Creative Director</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 mb-5">
              <div class="card" style="width: 20rem;">
                <img class="card-img-top" src="img/pexels2.jpeg" alt="Team member photo">
                <div class="card-body">
                  <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
                  <div class="card-text text-center status" style= "font-size: 12px;">Creative Director</div>
                </div>
              </div>
            </div>
        </div>
       
    </div>
</section>
<section id="testimony">
  <div class="container pt-5 pb-5">
      <h3 class="text-center  ">Testimonies</h3>
      <hr class="hr">
      <div class="row pt-4">
          <div class="col-md-4 mb-5">
            <div class="card" style="width: 20rem;">
              <center><img class="card-img-top pt-2 mb-2 test-image" src="img/pexels2.jpeg" alt="Team member photo"></center>
              <div class="card-body">
                  <p class= "text-center hyphen">"</p>
                <div class="card-text text-justify" style= "font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e est laborum.onsequat. Duis aute 
                    irure dolor in reprehenderit in voluptate velit e est laborum.</div>
                    <p class= "text-center hyphen">"</p>
                <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-5">
            <div class="card" style="width: 20rem;">
              <center><img class="card-img-top pt-2 mb-2 test-image" src="img/beard-boy.jpeg" alt="Team member photo"></center>
              <div class="card-body">
                  <p class= "text-center hyphen">"</p>
                <div class="card-text text-justify" style= "font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e est laborum.onsequat. Duis aute 
                    irure dolor in reprehenderit in voluptate velit e est laborum.</div>
                    <p class= "text-center hyphen">"</p>
                <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-5">
            <div class="card" style="width: 20rem;">
              <center><img class="card-img-top pt-2 mb-2 test-image" src="img/pexels.jpeg" alt="Card image cap"></center>
              <div class="card-body">
                <p class= "text-center hyphen">"</p>
                <div class="card-text text-justify" style= "font-size: 12px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e est laborum.onsequat. Duis aute 
                    irure dolor in reprehenderit in voluptate velit e est laborum.</div>
                    <p class= "text-center hyphen">"</p>
                <h6 class="card-text text-center font-weight-bold">Nelson Manny</h6>
              </div>
            </div>
          </div>

      </div>
     
  </div>
</section>
<section id="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                    <h3>Subscribe Now for Loaded Business Building Tips
                        </h3>
            </div>
            <div class="col-sm-2">
            <a hreg="#" button type="button" class="btn btn-subcribe d-sm-inline d-block ">Subscribe</a>
    </div>
        </div>
  </div>
</section> 

@endsection