@extends('layouts.master')

@section('content')
    <!-- Business Section -->
    <section id="story" >
        <div class="jumbotron-fluid">
            <div class="card car">
                <div class="row pl-md-5 pl-sm-0">
                    <div class="col-md-5 nextt">
                        <div class="card-body ">
                            <div class="row ">
                                <div class="col-md-5">
                                <p class="p-18"><a href="" class= "p-18" >MUST READ
                               <span> <i class="fa fa-angle-right" style="color:#33158C; font-size:24px; padding-left: 10px; "></i></span></a></p>
                            </div>

                              <div class="col-md-7"> <hr class="hr1"></div>
                            </div>
                            <h1 class="">{!! $featured[0]->title !!}</h1>
                            <p class="p-18">{{ Carbon::parse($featured[0]->created_at)->diffForHumans()}}</p>
                            <p><a href="{{ url('/blog/'.$featured[0]->slug) }}" class="btn read">Read Now</a></p>
                        </div>
                    </div>          
                    <div class="col"></div>        
                    <div class="col-md-6 d-lg-block d-md-block d-sm-none">
                        <img class="img-fluid card-img-top w-100" src="{{ $featured[0]->featured_image or asset('/uploads/blog.jpeg') }}" alt="Card image cap" style="height: 450px;">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Main Section -->

 <section id="container" class="">
      <div class="row border-bottom-1 middle-nav px-5">
            <div class="col-12 nav-scroller py-1 mb-2">
              <nav class="nav d-flex justify-content-between">
                  @foreach($cats as $c)
                  <a class="pt-3 p-1 text-muted text-capitalize" href="{{ url('/blog/category/'.$c->slug) }}">{!!$c->name !!}</a>
                @endforeach   
              </nav>
          </div>
          {{-- <div class="col-md-1 iconn">
               <span class="icon"><a href="" class="p-188 mr "><i class="fa fa-search"></i></a></span>
          </div>  --}}            
     </div>
</section>
<!-- end of middle nav -->

 <!-- Card -->
<section id="content">
        <div class="container">
                <div class="row pt-5 recent">
                      <p class="p-18 next"><a href="" class="p-18">Recent Posts</a></p>
                      <p class="p-18 next ml-auto"><a href="" class="p-18"> MORE
                      <span> <i class="fa fa-angle-right" style="color:#33158C; font-size:24px; padding-left: 10px; "></i></span></a></p>
                </div>
                <br>

                @foreach($newest->chunk(4) as $news)
                    <div class="card-deck pt-4">
                          @foreach($news as $new)
                            <div class="card">
                                <img class="card-img-top" src="{{$new->featured_image or asset('img/investment-1.png')}}" alt="Card image cap" height="200">
                                <div class="card-body">
                                  <h6 class="card-title">{!! $new->title !!}</h6>
                                  <p class="card-text small" style="margin-top: -5px; overflow: hidden; text-overflow: clip;">{!! str_limit($new->excerpt ,100)!!}</p>
                                  
                                  <a class="readd" href="{{ url('blog/'.$new->slug) }}">Learn More</a>             
                              </div>
                            </div>
                          @endforeach 
                     </div>
                @endforeach
        </div>

             <!-- End of card    -->

            <!-- Features -->

                    <div class="card mt-5 feature jumbotron-fluid ">
                         <div class="row">
                             <div class="col-md-6">
                             <img class="card-img-top" src="{{ $featured[1]->featured_image or asset('img/image 2.png') }}" alt="Card image cap" height="500px">
                            </div>
                            <div class="col-md-6">
                             <div class="card-body">
                              <p class="card-title p-188">Featured</p>
                              <p class="card-title p-24">{!! $featured[1]->title !!}</p>

                              <p class="card-text p-18a">{!! $featured[1]->excerpt !!}</p>
                            <br> <p><a href="{{ url('/blog/'.$featured[1]->slug) }}" class="btn read">Learn More</a></p>
                            
                            <div class="row pt-5">
                             <p class="p-18 mt-5 ">{{ Carbon::parse($featured[0]->created_at)->diffForHumans()}}</p>
                             <p class="p-18 next ml-auto mt-5"><a href="" class="p-18">MORE
                             <span> <i class="fa fa-angle-right" style="color:#33158C; font-size:24px; padding-left: 10px; "></i></span></a></p>
                            </div>
                            </div>
                        </div>
                          </div>
            </div>
            <!-- End of features -->
            <div class="container">
                <div class="row pt-5 recent">
                      <p class="p-18 next"><a href="" class="p-18">Old Posts</a></p>
                      <p class="p-18 next ml-auto"><a href="" class="p-18"> MORE
                      <span> <i class="fa fa-angle-right" style="color:#33158C; font-size:24px; padding-left: 10px; "></i></span></a></p>
                </div>
                <br>

                @foreach($oldest->chunk(4) as $olds)
                    <div class="card-deck pt-4">
                          @foreach($olds as $old)
                            <div class="card">
                                <img class="card-img-top" src="{{$old->featured_image or asset('img/investment-1.png')}}" alt="Card image cap" height="200">
                                <div class="card-body">
                                  <h6 class="card-title">{!! $old->title !!}</h6>
                                  <p class="card-text small" style="margin-top: -5px; overflow: hidden; text-overflow: clip;">{!! str_limit($old->excerpt ,100)!!}</p>
                                  
                                  <a class="readd" href="{{ url('blog/'.$old->slug) }}">Learn More</a>             
                              </div>
                            </div>
                          @endforeach 
                     </div>
                @endforeach
        </div>
    </section>
    <section id="subscribe" class="mt-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                            <h3>Subscribe Now for Loaded Business Building Tips
                                </h3>
                    </div>
                    <div class="col-sm-2">
                    <a hreg="#" button type="button" class="btn btn-subcribe d-sm-inline d-block ">Subscribe</a>
            </div>
                </div>
          </div>
        </section>

    @endsection