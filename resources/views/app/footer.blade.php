    <section id="footer">
        <div class="container">
            <div class="row pt-3">
                <div class="col-md-8  order-4">
                   <p class="">Copyright &copy 2018 BIZINTELNG NIGERIA LIMITED. All rights reserved </p>  
                </div>
                <div class="col-md-4 order-md-8">
                    <div class="social-icons">
                            <ul class="">
                                <li class="social"><a href="#"><i class="fa fa-twitter" style="color:#C4C4C4;"></i></a></li>
                                <li class="social"><a href="#"><i class="fa fa-facebook" style="color:#C4C4C4;"></i></a></li>
                                <li class="social"><a href="#"><i class="fa fa-google-plus" style="color:#C4C4C4;"></i></a></li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>