<section id="hero">  
<nav class="navbar navbar-expand-lg navbar-light bg-light pt-3 pb-3 ">
                <div class="header container">
                <a class="navbar-brand" href="{{ url('/') }}"><img class="img-fluid" width="100px"src="{{ asset('img/Bizintel-logo.svg') }}"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link {{Request::is('/') ? 'active' : '' }}" href="{{ url('/') }}">Home</a>
              </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('blog') ? 'active' : '' }}" href="{{ url('blog') }}">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{Request::is('services') ? 'active' : '' }}" href="{{ url('services') }}">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Request::is('about') ? 'active' : '' }}" href="{{ url('about') }}">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{Request::is('contact') ? 'active' : '' }}" href="{{ url('contact') }}">Contact Us</a>
              </li>
              {{-- <li class="nav-item">
                <a class="nav-link" href="#">Shop</a>
              </li> --}}                  
          </ul>
        </div>
    </div>
  </nav>
</section>