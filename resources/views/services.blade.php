@extends('layouts.master')

@section('content')
<style type="text/css">
  @media (min-width: 768px) {
      .modal-xl {
        width: 90%;
       max-width:1200px;
      }
    }
</style>
    
    <section class="h-100">
                <div class="jumbotron jumbotron-fluid service">
                    <div class="container pt-5 text-white">
                      <h1 class="display-4">Get Access to our amazing<br> offers and services<h1>
                     </div>
                </div>
     </section>

     <section id="finance" class="py-5">
            <div class="container pt-5">
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-fluid" src="uploads/finance.png">
                    </div>
                    <div class="col-md-6">
                        <h5 class="service-text pt-5 mt-5">Financing solutions for SMEs:</h5>
                        <p class="pt-5">Lack of access to finance is a major challenge facing legitimate businesses in Nigeria, particularly small businesses. We intend to tackle this challenge head-on by providing needed finance for small businesses in a way that enable them to scale their business and support the achievement of their business goal. We would in a confidential, efficient and structured manner leveraging, technology as much as possible. Do you need financing for your business, drop us a line.</p>
                     
                        <a href="#"  type="button" class="btn service-btn pt-3 mt-5 d-sm-inline d-block border-0" data-toggle="modal" data-target="#financeModal">Get Funding For Your Business</a>
                        @include('modal.finance')
                </div>
            </div>
        </section>
        
        <section id="advisory" class="py-5">
                <div class="container pt-5">
                    <div class="row">
                        <div class="col-md-6 order-6 ">
                            <h5 class="service-text pt-5 mt-5">Business advisory services for SMEs</h5>
                                <p class="pt-5">Nigeria business environment is a challenging one and ability to navigate the ecosystem is a difference between successful and failed businesses. We provide general and bespoke consulting services to businesses, particularly those in the early phase, to enable them to develope their business. In doing this, we leverage our over 5 years of business development services for hundreds of small businesses in Nigeria. We also offer business training for business owners and those willing to start a business.</p>
                                <a href="#" type="button" class="btn service-btn pt-3 mt-5 d-sm-inline d-block" data-toggle="modal" data-target="#adviseModal">Get mentorship for your Business</a>
                        </div>
                        @include('modal.advise')
                        <div class="col-md-6">
                                <img class="img-fluid " src="uploads/advisory.png">

                        </div>
                    </div>
                </div>
       </section>


       <section id="consultancy" class="bg-white py-5">
            <div class="container pt-5">
                <div class="row">
                    <div class="col-md-6 ">
                            <img class="img-fluid"  src="uploads/consultancy.png">
                        </div>
                    <div class="col-md-6 ">
                            <h5 class="service-text pt-5 mt-5">CSR consulting</h5>
                            <p class="pt-5">A well thought out corporate social responsibility programme can a business gain wider acceptance and credibility with her stakeholders. Such acceptance can help a business achieve its goals, for example, by increasing the sale of its products and or services.
                             We have cognate experience in supporting businesses to plan and deliver a fit for purpose  corporate social responsibility project or programme
                                    </p>
                            <a href="#" button type="button" class="btn service-btn pt-3 mt-5 d-sm-inline d-block" data-toggle="modal" data-target="#consultancyModal">Hire a Consultant</a>
                            @include('modal.consultancy')
                    </div>
                </div>
            </div>
        </section>
        <section id="subscribe">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                            <h3>Subscribe Now for Loaded Business Building Tips
                                </h3>
                    </div>
                    <div class="col-sm-2">
                    <a hreg="#" button type="button" class="btn btn-subcribe d-sm-inline d-block ">Subscribe</a>
            </div>
                </div>
          </div>
        </section>         
         
     @endsection                  